package TrabajadorEjercicio3;

public class IngresoHorasExtra extends Ingreso{
	
	private int cantidadDeHorasExtra;

	public IngresoHorasExtra(String mes, String concepto, Double monto, int cantidadDeHorasExtra) {
		super(mes, concepto, monto);
		this.setCantidadDeHorasExtra(cantidadDeHorasExtra);
	}

	public int getCantidadDeHorasExtra() {
		return cantidadDeHorasExtra;
	}

	public void setCantidadDeHorasExtra(int cantidadDeHorasExtra) {
		this.cantidadDeHorasExtra = cantidadDeHorasExtra;
	}
	
    @Override
	
    public double montoImponible() {
    	return 0;
    }

}
