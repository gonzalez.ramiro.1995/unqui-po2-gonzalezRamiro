package TrabajadorEjercicio3;

import java.util.ArrayList;
import java.util.List;

public class Trabajador {

	private Ingreso ingresoDelMes;
	private List<Ingreso> ingresosDelAnio;
	
	
	public Trabajador(Ingreso unIngreso) {
		this.setIngresoDelMes(unIngreso);
		ingresosDelAnio = new ArrayList<Ingreso>();
	}


	public Ingreso getIngresoDelMes() {
		return ingresoDelMes;
	}


	public void setIngresoDelMes(Ingreso ingresoDelMes) {
		this.ingresoDelMes = ingresoDelMes;
	}
	
	
	
	
	public List<Ingreso> getIngresosDelAnio() {
		return ingresosDelAnio;
	}


	public Double getTotalPercibido() {
		Double totalADevolver = 0d;
		
		for(Ingreso ingreso : this.getIngresosDelAnio()) {
			totalADevolver = totalADevolver + ingreso.getMonto();
		}
		
		return totalADevolver;
	}
	
	
	public void agregarIngresoALista(Ingreso unIngreso) {
		this.ingresosDelAnio.add(unIngreso);
	}
	
	public Double getMontoImponible() {
		return this.getIngresoDelMes().montoImponible();
	}
	
	public Double getImpuestoAPagar() {
		return this.ingresoDelMes.getMonto() * (0.02);
	}
}
