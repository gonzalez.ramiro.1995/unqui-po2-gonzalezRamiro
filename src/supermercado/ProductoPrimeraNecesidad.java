package supermercado;

public class ProductoPrimeraNecesidad extends Producto {
    
	private int descuento;
	
	
	public ProductoPrimeraNecesidad(String nombreProducto, Double precio , boolean esDePreciosCuidados , int descuento) {
		super(nombreProducto, precio, esDePreciosCuidados);
        this.setDescuento(descuento);
		
	}

	
	
	
   
  

   public int getDescuento() {
	   return descuento;
   }




	public void setDescuento(int descuento) {
		
		this.descuento = descuento;
	}



   
	private Double montoADescontar() {
		
		return 1 - (double)this.getDescuento() / 100;
	}


	@Override
	public Double getPrecio() {
		
		
		
	 return this.precio  * this.montoADescontar() ;
		
	
	}
	
	
	
}
