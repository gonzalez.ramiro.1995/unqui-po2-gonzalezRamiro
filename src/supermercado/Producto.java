package supermercado;

import java.util.function.BooleanSupplier;

public class Producto {

	private String nombre;
	protected Double precio;
	private boolean esDePreciosCuidados = false;
	
	
	public Producto(String nombreProducto , Double precio , boolean esPreciosCuidados) {
		this.setNombre(nombreProducto);
		this.setPrecio(precio);
		this.setEsDePreciosCuidados(esPreciosCuidados);
	}
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
	
	
	public void setEsDePreciosCuidados(boolean esDePreciosCuidados) {
		this.esDePreciosCuidados = esDePreciosCuidados;
	}


	public void hacerProductoDePrimeraNecesidad() {
		this.esDePreciosCuidados = true;
	}


	public void aumentarPrecio(double d) {
		this.precio = this.precio + d;
		
	}


	public boolean esPrecioCuidado() {
		
		return this.esDePreciosCuidados;
	}
	
}
