package supermercado;

import java.util.ArrayList;
import java.util.List;

public class Supermercado {

	private String nombre;
	private String direccion;
	private List<Producto> productosALaVenta;
	
	
	public Supermercado(String nombre , String direccion) {
		this.setNombre(nombre);
		this.setDireccion(direccion);
		this.productosALaVenta = new ArrayList<Producto>();
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public List<Producto> getProductosALaVenta() {
		return productosALaVenta;
	}


	public void setProductosALaVenta(List<Producto> productosALaVenta) {
		this.productosALaVenta = productosALaVenta;
	}
	
	public void agregarProducto(Producto unProducto) {
		this.productosALaVenta.add(unProducto);
		
	}
	
	public int getCantidadDeProductos() {
		
		return this.productosALaVenta.size();
	}
	
    public Double getPrecioTotal() {
    	double precioAdevolver = 0;
    	
    	for(Producto producto : this.getProductosALaVenta()) {
    		precioAdevolver = precioAdevolver + producto.getPrecio();
    	}

    	return precioAdevolver;
    }
	
}
