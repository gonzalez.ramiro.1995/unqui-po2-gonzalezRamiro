package TP6;

public class SolicitudCreditoPersonal extends SolicitudDeCredito{

	
	public SolicitudCreditoPersonal(Cliente solicitante, Double monto , int meses) {
		
		this.setSolicitante(solicitante);
		this.setMontoSolicitado(monto);
		this.setPlazoMeses(meses);
	}
	
	
	public boolean solicitanteCumpleConRequisitoDeSueldo() {
		return this.getSolicitante().getSueldoAnual() >= 15000;
	}
	

	
	public boolean solicitanteCumpleConRequisitoDeCuota() {
		return this.cuotaMensual() <= (this.getSolicitante().getSueldoMensual() * 0.70);
	}
	
	
	@Override
	public boolean esAceptable() {
		
		return this.solicitanteCumpleConRequisitoDeCuota() && this.solicitanteCumpleConRequisitoDeSueldo();
	}
	
	

}
