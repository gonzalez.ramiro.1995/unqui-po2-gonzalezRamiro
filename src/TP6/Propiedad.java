package TP6;

public class Propiedad {
	
	private String descripcion;
	private String direccion;
	private Double valor;
	
	public Propiedad(String descripcion , String direccion , Double valor) {
		this.setDescripcion(descripcion);
		this.setDireccion(direccion);
		this.setValor(valor);
		
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}

	
	
	


}
