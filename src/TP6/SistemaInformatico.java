package TP6;

public interface SistemaInformatico {
	
   abstract public void agregarCliente(Cliente cliente);
   abstract public void agregarSolicitud(SolicitudDeCredito solicitud);
   abstract public Double montoADesembolsar();
   
}
