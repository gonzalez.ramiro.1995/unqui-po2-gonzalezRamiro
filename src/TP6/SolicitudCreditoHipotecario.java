package TP6;

public class SolicitudCreditoHipotecario extends SolicitudDeCredito{
	
	private Propiedad propiedadDelSolicitante;

	public SolicitudCreditoHipotecario(Cliente solicitante, Double monto , int meses , Propiedad unaPropiedad) {
		
		this.setSolicitante(solicitante);
		this.setMontoSolicitado(monto);
		this.setPlazoMeses(meses);
		this.setPropiedadDelSolicitante(unaPropiedad);
	}
	
	
	
	
	public Propiedad getPropiedadDelSolicitante() {
		return propiedadDelSolicitante;
	}




	public void setPropiedadDelSolicitante(Propiedad propiedadDelSolicitante) {
		this.propiedadDelSolicitante = propiedadDelSolicitante;
	}

    
	public boolean solicitanteCumpleRequerimientoDeCuota() {
		return this.cuotaMensual() <= (this.getSolicitante().getSueldoMensual() / 2);
		
	}
	
	public boolean solicitanteCumpleRequerimientoDeMonto() {
		return this.getMontoSolicitado() <= (this.getPropiedadDelSolicitante().getValor() * 0.7);
		
	}

	public boolean solicitanteCumpleRequerimientoDeEdad() {
		return this.getSolicitante().getEdad() + (this.getPlazoMeses() /12) <= 65;
	}
	
	@Override
	public boolean esAceptable() {
		
		return this.solicitanteCumpleRequerimientoDeCuota() && 
			   this.solicitanteCumpleRequerimientoDeEdad() && 
			   this.solicitanteCumpleRequerimientoDeMonto();
	}
	
	

}
