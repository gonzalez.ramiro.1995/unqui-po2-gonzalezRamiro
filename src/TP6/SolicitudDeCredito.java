package TP6;

public abstract class SolicitudDeCredito {
    
	private Cliente solicitante;
	private Double montoSolicitado;
	private int plazoMeses;
	
	
	
	public Cliente getSolicitante() {
		return solicitante;
	}



	public void setSolicitante(Cliente solicitante) {
		this.solicitante = solicitante;
	}



	public Double getMontoSolicitado() {
		return montoSolicitado;
	}



	public void setMontoSolicitado(Double montoSolicitado) {
		this.montoSolicitado = montoSolicitado;
	}



	public int getPlazoMeses() {
		return plazoMeses;
	}



	public void setPlazoMeses(int plazoMeses) {
		this.plazoMeses = plazoMeses;
	}


    public Double cuotaMensual() {
    	return this.montoSolicitado / this.plazoMeses;
    }
	
	public abstract boolean esAceptable();
}
