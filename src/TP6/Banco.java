package TP6;

import java.util.ArrayList;
import java.util.List;

public class Banco implements SistemaInformatico{
   
	private List<Cliente> clientes;
	private List<SolicitudDeCredito> listaDeSolicitudes;
	
	public Banco() {
		this.clientes = new ArrayList<Cliente>();
		this.listaDeSolicitudes = new ArrayList<SolicitudDeCredito>();
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	

	public List<SolicitudDeCredito> getListaDeSolicitudes() {
		return listaDeSolicitudes;
	}

	@Override
	public void agregarCliente(Cliente cliente) {
		this.clientes.add(cliente);
		
	}

	@Override
	public void agregarSolicitud(SolicitudDeCredito solicitud) {
		this.listaDeSolicitudes.add(solicitud);
		
	}
	

	@Override
	public Double montoADesembolsar() {
	
		
	  return  this.getListaDeSolicitudes()
		    .stream()
		    .filter(s -> s.esAceptable())
		    .mapToDouble(s -> s.getMontoSolicitado())
		    .sum();
		    
		
		
	}
	
    
}
