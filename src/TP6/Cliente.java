package TP6;

public class Cliente {

 	private String nombre;
 	private String apellido;
 	private String direccion;
 	private int edad;
 	private Double sueldoMensual;
 	
 	
 	public Cliente(String nombre, String apeelido, String direccion , int edad, Double sueldoMensual) {
 		this.setNombre(nombre);
 		this.setDireccion(direccion);
 		this.setDireccion(direccion);
 		this.setEdad(edad);
 		this.setSueldoMensual(sueldoMensual);
 	}
 	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public Double getSueldoMensual() {
		return sueldoMensual;
	}
	public void setSueldoMensual(Double sueldoMensual) {
		this.sueldoMensual = sueldoMensual;
	}
 	
 	public Double getSueldoAnual() {
 		return this.sueldoMensual * 12;
 	}
 	
	
}
