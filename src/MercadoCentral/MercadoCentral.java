package MercadoCentral;

import java.util.List;



public class MercadoCentral implements Agencia{

	
	private List<ProductoM> productosEnStock;
    

	public MercadoCentral(List<ProductoM> productos) {
		
		this.setProductosEnStock(productos);
	}
	


	public List<ProductoM> getProductosEnStock() {
		return productosEnStock;
	}

	public boolean hayStockDelProducto(ProductoM unProducto) {
		
		return this.getProductosEnStock().contains(unProducto);
	}

	public void setProductosEnStock(List<ProductoM> productosEnStock) {
		this.productosEnStock = productosEnStock;
	}
	
	
	
	private void decrementarStock(ProductoM unProducto) {
		this.productosEnStock.remove(unProducto);
		
	}

	
	public Double registrarComprasDeCliente(List<ProductoM> productosDelCliente) {
		Double montoAPagar = 0d;
		
		for(ProductoM producto : productosDelCliente) {
			montoAPagar = montoAPagar + producto.costo();
			this.decrementarStock(producto);
		}
		
		return montoAPagar;
	}
	
	
	public void pagoDeFacturas(Factura factura) {
		this.registrarPago(factura);
		
		
	}
	
	public int cantidadDeStock(ProductoM unProducto) {  
		int contador = 0;
		
		for(ProductoM producto : this.getProductosEnStock()) {
			
			if(producto == unProducto) {
				contador++;
			}
			
		}
		return contador;
	}



	@Override
	public void registrarPago(Factura factura) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}