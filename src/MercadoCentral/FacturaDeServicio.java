package MercadoCentral;

public class FacturaDeServicio extends Factura {
	
	private Double costoPorUnidad;
	private Double unidadesConsumidas;
	
    

	public FacturaDeServicio(Double costoPorUnidad, Double unidadesConsumidas) {
        this.setCosto(costoPorUnidad);
        this.setCosto(unidadesConsumidas);
	}



	public Double getCostoPorUnidad() {
		return costoPorUnidad;
	}



	public void setCostoPorUnidad(Double costoPorUnidad) {
		this.costoPorUnidad = costoPorUnidad;
	}



	public Double getUnidadesConsumidas() {
		return unidadesConsumidas;
	}



	public void setUnidadesConsumidas(Double unidadesConsumidas) {
		this.unidadesConsumidas = unidadesConsumidas;
	}



	@Override
	public Double getCosto() {
		return this.getCostoPorUnidad() * this.getUnidadesConsumidas();
	}

}
