package TP6Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import TP6.Banco;
import TP6.Cliente;
import TP6.SolicitudCreditoPersonal;

class BancoTest {
    
	Banco unBanco;
	Cliente unCliente;
	SolicitudCreditoPersonal unaSolicitud;
	SolicitudCreditoPersonal otraSolicitud;
	
	
	@BeforeEach
	public void setup() {
		unBanco = new Banco();
		unCliente = new Cliente ("Ramiro" , "Gonzalez" , "Quilmes" , 25 , 40000d);
		unaSolicitud = new SolicitudCreditoPersonal(unCliente , 10000d , 12);
		otraSolicitud = new SolicitudCreditoPersonal(unCliente , 10000000d , 12);
		
		
	}
	
	@Test
	public void montotoalADesembolsar() {
		unBanco.agregarSolicitud(unaSolicitud);
		unBanco.agregarSolicitud(unaSolicitud);
		unBanco.agregarSolicitud(otraSolicitud);
		
		assertEquals(20000d , unBanco.montoADesembolsar());
	}
	

}
