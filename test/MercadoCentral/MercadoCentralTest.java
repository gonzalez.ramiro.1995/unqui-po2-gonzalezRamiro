package MercadoCentral;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



class MercadoCentralTest {
	
	
	ProductoM prod1 = new ProductoM(12d);
	ProductoM prod2 = new ProductoM(10d);
	ProductoM prod3 = new ProductoMCooperativa(15d);
	ProductoM prod4 = new ProductoMCooperativa(16d);
	List<ProductoM> stockDeProductos = new ArrayList<ProductoM>();
	
	List<ProductoM> productosDeCliente = new ArrayList<ProductoM>();
	
	MercadoCentral unMercado = new MercadoCentral(stockDeProductos);

	@BeforeEach
	public void setup() {
		
		stockDeProductos.add(prod1);
		stockDeProductos.add(prod1);
		stockDeProductos.add(prod2);
		stockDeProductos.add(prod3);
		
		
		
		productosDeCliente.add(prod1);
		productosDeCliente.add(prod2);

     }
	
	
	@Test
	public void testHayStockDeProducto() {
		assertTrue(unMercado.hayStockDelProducto(prod1));
		
	}
	
	@Test
	public void testNoHayStockDeProducto() {
		assertTrue(!unMercado.hayStockDelProducto(prod4));
		
	}
	
	@Test
	public void montoAPagarPorElCliente() {
		
		assertEquals(22d , unMercado.registrarComprasDeCliente(productosDeCliente));
	}
	

	@Test
	public void montoAPagarPorElCliente_N() {
		
		assertNotEquals(23d , unMercado.registrarComprasDeCliente(productosDeCliente));
	}
	
	
	@Test
	public void decrementarStockDespuesDeCompra() {
		
		assertEquals(22d , unMercado.registrarComprasDeCliente(productosDeCliente));
		assertEquals(1, unMercado.cantidadDeStock(prod1));
		assertEquals(0, unMercado.cantidadDeStock(prod2));

	}
	
}


