package Ejercicio3Test;
import TrabajadorEjercicio3.Ingreso;
import TrabajadorEjercicio3.Trabajador;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;



class TrabajadorTest {
	
	Ingreso unIngreso = new Ingreso("Agosto","Trabajo",120d);
	Trabajador trabajador = new Trabajador(unIngreso);
	

	@Test
	void testGetTotalPercibido() {
		trabajador.agregarIngresoALista(unIngreso);
		assertEquals((double)120 , trabajador.getTotalPercibido());
		
		trabajador.agregarIngresoALista(unIngreso);
		assertEquals((double)240 , trabajador.getTotalPercibido());

	}

	
	@Test
	void testGetMontoImponible() {
		assertEquals((double)120 , trabajador.getIngresoDelMes().montoImponible());
		
	}
	
	
	@Test
	void testGetImpuestoAPagar() {
		assertEquals((double)2.4 , trabajador.getImpuestoAPagar());
	}
}
